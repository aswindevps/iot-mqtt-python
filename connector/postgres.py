import psycopg2

conn = ''
class PostgresDB:
    def connect(self):
        global conn
        conn = psycopg2.connect("dbname='iot_sensor_db' user='robocet' host='18.219.220.211' password='robocet123'")
        if(conn):
            print ("\nPostgres Connected")
            return conn

    def query(self,queryString):
        cur = conn.cursor()
        cur.execute(queryString)
        rows = cur.fetchall()
        conn.commit()
        cur.close()
        return rows

    def insert(self,sql,mac_id,temperature,timestamp):
        cur = conn.cursor()
        try:
            cur.execute(sql,(mac_id,temperature,timestamp),)
            conn.commit()
            cur.close()
        except:
            print ("I can't INSERT")