import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.publish('temp/25:45:DF:SD:LL', payload=50, qos=0, retain=False)

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")

client = mqtt.Client(transport="websockets")
client.username_pw_set("mqmobile", password="]pm5L]6aL7'MBhp~")

client.on_connect = on_connect
client.on_disconnect = on_disconnect

client.connect('mqtt.hospifi.co',8083,60)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()