import paho.mqtt.client as mqtt
from connector.postgres import PostgresDB
import datetime

def connectDB():
    global postgresInstance
    postgresInstance = PostgresDB()
    postgresInstance.connect()

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("\nMQTT Connected\n")

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("temp/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    # print (msg.topic, msg.payload.decode("utf-8"))
    mac_id = msg.topic.split('/')[1]
    temperature = int(msg.payload.decode("utf-8"))
    timestamp = datetime.datetime.now()
    sql="INSERT INTO temperature(mac_id,temp,timestamp) VALUES(%s,%s,%s)"
    postgresInstance.insert(sql,mac_id,temperature,timestamp)

    print(mac_id, temperature, timestamp)

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")

# Connect Postgres DB
connectDB()

#MQTT
client = mqtt.Client(transport="websockets")
client.username_pw_set("mqmobile", password="]pm5L]6aL7'MBhp~")

client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

client.connect('mqtt.hospifi.co',8083,60)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()